#!/usr/bin/env python3

from subprocess import call, check_output
import os
import sys
import shutil
import librosa
import numpy as np
from io import BytesIO
from scipy.io.wavfile import read
from PIL import Image

# Prepare a Caffe index file

def prepare_index(workdir_path, id_file, dst_file, label_dict):
  indexfile = open(dst_file, "w")
  idfile = open(id_file)

  for line in idfile:
    word = line.split(' ')[1].strip()
    wid = line.split(' ')[0].strip()

    if word in label_dict.keys():
      label = label_dict[word]
      ifile_line = os.path.abspath(os.path.join(workdir_path, wid.strip())) + " " + label + "\n"
      indexfile.write(ifile_line)

  indexfile.close()
  idfile.close()

def prepare_split_files(workdir_path, label_dict, train_split, val_split, test_split, train_dst_file, val_dst_file, test_dst_file):
  train_splitfile = open(train_dst_file, "w")
  val_splitfile = open(val_dst_file, "w")
  test_splitfile = open(test_dst_file, "w")

  for label in label_dict:
    datapath = "/".join(label.split('.'))
    fullpath = os.path.join(workdir_path, datapath)
    files = os.listdir(fullpath)
    train_split_count = int(round(len(files) * (train_split / 100)))
    val_split_count = int(round(len(files) * (val_split / 100)))
    test_split_count = int(round(len(files) * (test_split / 100)))
    train_files = files[:train_split_count]
    val_files = files[train_split_count:train_split_count+val_split_count]
    test_files = files[train_split_count+val_split_count:]

    for f in train_files:
      train_splitfile.write(os.path.join(datapath, f.strip()) + " " + label + "\n")

    for f in val_files:
      val_splitfile.write(os.path.join(datapath, f.strip()) + " " + label + "\n")

    for f in test_files:
      test_splitfile.write(os.path.join(datapath, f.strip()) + " " + label + "\n")

  train_splitfile.close()
  val_splitfile.close()
  test_splitfile.close()

if __name__ == "__main__":

  archives = ["images_background", "images_evaluation"]

  url = "https://github.com/brendenlake/omniglot/raw/master/python/"

  data_path = 'data'

  train_split = 80
  val_split = 10
  test_split = 10

  if not os.path.exists(data_path):
    os.makedirs(data_path)

  print("Downloading dataset...")

  for archive in archives:
    if not os.path.exists(archive+".zip"):
      call(
        "wget "+url+archive+".zip",
        shell=True
      )

  print("Downloading done.")

  print("Extracting...")

  for archive in archives:
    call(
      "unzip " + archive+".zip" + " -d " + data_path,
      shell=True
    )

  for archive in archives:
    call(
      "mv " + data_path + "/" + archive + "/* " + data_path + "/",
      shell=True
    )
    call(
      "rm -rf " + data_path + "/" + archive,
      shell=True
    )

  print("Extracting successfully done to {}".format(data_path))

  print("Converting...")

  labels_path = 'labels.txt'
  labels_file = open(labels_path, "w")

  labels = []
  scripts = [f for f in os.listdir(data_path)]
  for script in scripts:
    chars = os.listdir(data_path + "/" + script)
    for char in chars:
      labels.append(script+"."+char)

  for label in labels:
    print(label, file=labels_file)

  labels_file.close()

  label_index = [str(labels.index(l)) for l in labels]
  label_dict = dict(zip(labels, label_index))

  print("Preparing train/validation/test splits...")

  prepare_split_files(data_path, label_dict,
                      train_split, val_split, test_split,
                      os.path.join(data_path, "training_list.txt"),
                      os.path.join(data_path, "validation_list.txt"),
                      os.path.join(data_path, "testing_list.txt"))

  val_index = os.path.join(data_path, 'validation-index.txt')
  val_id_file = os.path.join(data_path, 'validation_list.txt')

  print("Preparing validation set...")

  prepare_index(data_path, val_id_file, val_index, label_dict)

  print("Done.")

  test_index = os.path.join(data_path, 'test-index.txt')
  test_id_file = os.path.join(data_path, 'testing_list.txt')

  print("Preparing test set...")

  prepare_index(data_path, test_id_file, test_index, label_dict)

  print("Done.")

  train_index = os.path.join(data_path, 'train-index.txt')
  train_id_file = os.path.join(data_path, 'training_list.txt')

  print("Preparing training set...")

  prepare_index(data_path, train_id_file, train_index, label_dict)

  print("Done.")
