#!/usr/bin/env python3

from subprocess import call, check_output
import os
import sys
import shutil
import librosa
import numpy as np
from io import BytesIO
from scipy.io.wavfile import read
from PIL import Image

# Compute the MFCC spectrogram of the audio, save it as an image

def compute_mfcc(wavFilePath, imgPath):
  n_freq = 40 # number of frequency bands
  n_window = 44 # number of time slices
  s_rate = 16000 # audio sample rate in WAV files

  f = open(wavFilePath, "rb")
  buf = BytesIO(f.read())
  wav = read(buf)
  x = np.array(wav[1], dtype=float)
  mfccs = librosa.feature.mfcc(x, sr=s_rate, n_mfcc=n_freq)
  f.close()

  # check mfcc dimensions
  if mfccs.shape[0] != n_freq:
    print("MFCC matrix has non-standard frequency bands: %d" % mfccs.shape[0])
    sys.exit(1)

  if mfccs.shape[1] < n_window:
    t = n_window - mfccs.shape[1]
    p = np.zeros(shape=(n_freq, t))
    mfccs = np.concatenate((mfccs, p), axis=1)
  elif mfccs.shape[1] > n_window:
    mfccs = mfccs[:, 0:n_window]

  mfcc_data = mfccs[:, :]
  mfcc_max_intensity = np.max(mfcc_data)
  mfcc_data = (mfcc_data * 255.0) / (mfcc_max_intensity)

  img = Image.fromarray(np.asarray(np.clip(mfcc_data, 0, 255), dtype="uint8"), "L")
  img.save(imgPath)

# Prepare a Caffe index file

def prepare_index(workdir_path, wav_id_file, dst_file, word_dict):
  indexfile = open(dst_file, "w")
  idfile = open(wav_id_file)

  for line in idfile:
    word = line.split('/')[0]
    wid = line[:-1].split('/')[1]

    if word in word_dict.keys():
      label = word_dict[word]
      # Compute the spectrogram and save to PNG
      compute_mfcc(os.path.join(workdir_path, line.strip()), os.path.join(workdir_path, line.strip()+".png"))
      indexfile.write(os.path.abspath(os.path.join(workdir_path, line.strip()))+".png" + " " + label + "\n")
      print(os.path.abspath(os.path.join(workdir_path, line.strip()))+".png")

  indexfile.close()
  idfile.close()

if __name__ == "__main__":

  archive = "speech_commands_v0.01.tar.gz"
  url = "http://download.tensorflow.org/data/"+archive

  data_path = 'data'

  if not os.path.exists(data_path):
    os.makedirs(data_path)

  print("Downloading dataset...")

  if not os.path.exists(archive):
    call(
      "wget "+url,
      shell=True
    )

  print("Downloading done.")

  print("Extracting...")

  call(
    "tar -zxvf " + archive + " -C " + data_path,
    shell=True
  )

  print("Extracting successfully done to {}".format(data_path))

  print("Converting...")

  labels_path = 'labels.txt'
  labels_file = open(labels_path, "r")
  labels = [x.strip() for x in labels_file.readlines()]
  labels_file.close()
  word_list = ['yes','no','up','down','left','right','on','off','stop','go']
  word_index = [str(labels.index(w)) for w in word_list]
  word_dict = dict(zip(word_list, word_index))

  train_index = os.path.join(data_path, 'train-index.txt')

  val_index = os.path.join(data_path, 'validation-index.txt')
  val_id_file = os.path.join(data_path, 'validation_list.txt')

  test_index = os.path.join(data_path, 'test-index.txt')
  test_id_file = os.path.join(data_path, 'testing_list.txt')

  print("Preparing validation set...")

  prepare_index(data_path, val_id_file, val_index, word_dict)

  print("Done.")

  print("Preparing test set...")

  prepare_index(data_path, test_id_file, test_index, word_dict)

  print("Done.")

  # We have to compute the training set as the difference between the
  # union of the testing and validation sets, and the entire fileset

  print("Preparing training set...")

  all_files = []

  for w in word_list:
    all_files += [x[5:] for x in check_output (
      "find " + os.path.join(data_path, w) + " -maxdepth 2 -iname *.wav",
      shell=True
    ).splitlines()]

  test_files = [x.split()[0] for x in open(test_index, "r").readlines()]
  val_files = [x.split()[0] for x in open(val_index, "r").readlines()]
  train_files = list(set(all_files) - (set(test_files).union(set(val_files))))

  train_id_file = os.path.join(data_path, 'training_list.txt')

  with open(train_id_file, "w") as f:
    for x in train_files:
      f.write(x.decode('utf-8')+'\n')

  prepare_index(data_path, train_id_file, train_index, word_dict)
  print("Done.")
