.PHONY: update update-caffe-cpu update-caffe-gpu all datasets scenarios transforms clean

update:
	yay -Syy trinnity-optimizer-git --answerclean A --answerdiff N
	yay -Syy trinnity-compiler-git --answerclean A --answerdiff N
	yay -Syy trinnity-git --answerclean A --answerdiff N

update-caffe-cpu:
	yay -Syy trinnity-caffe-git --answerclean A --answerdiff N

update-caffe-gpu:
	yay -Syy trinnity-caffe-cudnn-git --answerclean A --answerdiff N

all: scenarios transforms

clean: datasets-clean benchmarks-clean scenarios-clean transforms-clean

# Build topology graphs for Caffe models
%-topologies:
	for x in `ls models/$*`; do $(MAKE) -C models/$*/$$x $$x.topology; done

# Compile the microbenchmarking scenarios descriptor files
scenarios: cifar10-scenarios imagenet-scenarios kws-scenarios

%-scenarios: %-topologies
	@echo "# The order of parameters is: KERNELS, CHANNELS, STRIDE, WIDTH, HEIGHT, K_W, K_H, SPARSITY" > $*-scenarios
	@echo "declare -A SCENARIO_PARAMETERS" >> $*-scenarios
	@echo "SCENARIO_PARAMETERS=(\\" >> $*-scenarios
	@cat models/$*/*/*.topology | grep "Scenario" \
  | sort --version-sort | uniq \
  | awk '{ printf "%d %d %d %d %d %d %d %d\n", $$4, $$7, $$10, $$13, $$16, $$19, $$22, $$24 }' \
  | awk '($$6 % 2 == 1 || $$7 % 2 == 1) { printf "%d %d %d %d %d %d %d %d\n", $$1, $$2, $$3, $$4, $$5, $$6, $$7, $$8 }' \
  | grep -v "0 0 0 0" \
  | awk '{ printf "  [%d]=\"%d %d %d %d %d %d %d %d\"\n", NR-1, $$1, $$2, $$3, $$4, $$5, $$6, $$7, $$8 }' >> $*-scenarios
	@echo ")" >> $*-scenarios

%-scenarios-clean:
	rm -rf $*-scenarios

scenarios-clean:
	rm -rf *-scenarios

# Compile the microbenchmarking transforms descriptor files
transforms: cifar10-transforms imagenet-transforms kws-transforms

%-transforms: %-topologies
	@echo "# The order of parameters is: KERNELS, CHANNELS, STRIDE, WIDTH, HEIGHT, K_W, K_H, SPARSITY" > $*-transforms
	@echo "declare -A SCENARIO_PARAMETERS" >> $*-transforms
	@echo "SCENARIO_PARAMETERS=(\\" >> $*-transforms
	@cat models/$*/*/*.topology | grep "Scenario" \
	| awk '{ printf "%d %d %d %d %d %d %d %d\n", $$4, $$7, $$10, $$13, $$16, $$19, $$22, $$24 }' \
  | grep -v "0 0 0 0" \
  | awk '{ printf "  [%d]=\"%d %d %d %d %d %d %d %d\"\n", NR-1, $$1, $$2, $$3, $$4, $$5, $$6, $$7, $$8 }' >> $*-transforms
	@echo ")" >> $*-transforms

transforms-clean:
	rm -rf *-transforms

# Fetch the datasets
datasets: datasets-cifar10 datasets-imagenet datasets-kws

%-datasets:
	@cd datasets/$* && python get.py

datasets-clean:
	@rm -rf datasets/*/*.gz datasets/*/data

models/%.csv: %-benchmarks
	make -C models $*.csv

# Build and run benchmarks for host
%-benchmarks:
	$(MAKE) armcl-benchmarks-$*
	$(MAKE) mkldnn-benchmarks-$*
	$(MAKE) triNNity-benchmarks-$*
	$(MAKE) caffe-benchmarks-$*
	$(MAKE) tensorflow-benchmarks-$*

armcl-benchmarks-%:
	for model in $(shell ls models/$*); do $(MAKE) -B -C models/$*/$$model armcl.dat; done

mkldnn-benchmarks-%:
	for model in $(shell ls models/$*); do $(MAKE) -B -C models/$*/$$model mkldnn.dat; done

triNNity-benchmarks-%:
	for model in $(shell ls models/$*); do $(MAKE) -B -C models/$*/$$model triNNity.pgo.dat; done

triNNity-heuristic-benchmarks-%:
	for model in $(shell ls models/$*); do $(MAKE) -B -C models/$*/$$model triNNity-heuristic.pgo.dat; done

triNNity-sum2d-benchmarks-%:
	for model in $(shell ls models/$*); do $(MAKE) -B -C models/$*/$$model triNNity-sum2d.pgo.dat; done

triNNity-projected-%:
	for model in $(shell ls models/$*); do $(MAKE) -B -C models/$*/$$model triNNity.projected.dat; done

triNNity-heuristic-projected-%:
	for model in $(shell ls models/$*); do $(MAKE) -B -C models/$*/$$model triNNity-heuristic.projected.dat; done

triNNity-sum2d-projected-%:
	for model in $(shell ls models/$*); do $(MAKE) -B -C models/$*/$$model triNNity-sum2d.projected.dat; done

caffe-benchmarks-%:
	for model in $(shell ls models/$*); do $(MAKE) -B -C models/$*/$$model caffe.dat; done

tensorflow-benchmarks-%:
	for model in $(shell ls models/$*); do $(MAKE) -B -C models/$*/$$model tflow.dat; done

# Build and run benchmarks for ARM (via cross-compilation)
%-cross-benchmarks:
	$(MAKE) cross-armcl-benchmarks-$*
	$(MAKE) cross-mkldnn-benchmarks-$*
	$(MAKE) cross-triNNity-benchmarks-$*
	$(MAKE) cross-caffe-benchmarks-$*
	$(MAKE) cross-tensorflow-benchmarks-$*

cross-armcl-benchmarks-%:
	-rsync --quiet -avP --delete --ignore-errors ./ $(CROSS_BENCHMARK_HOST):$(CROSS_BENCHMARK_PATH)/
	for model in $(shell ls models/$*); do $(MAKE) -B -C models/$*/$$model armcl.cross.dat; done

cross-mkldnn-benchmarks-%:
	-rsync --quiet -avP --delete --ignore-errors ./ $(CROSS_BENCHMARK_HOST):$(CROSS_BENCHMARK_PATH)/
	for model in $(shell ls models/$*); do $(MAKE) -B -C models/$*/$$model mkldnn.cross.dat; done

cross-triNNity-benchmarks-%:
	-rsync --quiet -avP --delete --ignore-errors ./ $(CROSS_BENCHMARK_HOST):$(CROSS_BENCHMARK_PATH)/
	for model in $(shell ls models/$*); do $(MAKE) -B -C models/$*/$$model triNNity.cross.pgo.dat; done

cross-triNNity-heuristic-benchmarks-%:
	for model in $(shell ls models/$*); do $(MAKE) -B -C models/$*/$$model triNNity-heuristic.cross.pgo.dat; done

cross-triNNity-sum2d-benchmarks-%:
	for model in $(shell ls models/$*); do $(MAKE) -B -C models/$*/$$model triNNity-sum2d.cross.pgo.dat; done

cross-caffe-benchmarks-%:
	-rsync --quiet -avP --delete --ignore-errors ./ $(CROSS_BENCHMARK_HOST):$(CROSS_BENCHMARK_PATH)/
	for model in $(shell ls models/$*); do $(MAKE) -B -C models/$*/$$model caffe.cross.dat; done

cross-tensorflow-benchmarks-%:
	-rsync --quiet -avP --delete --ignore-errors ./ $(CROSS_BENCHMARK_HOST):$(CROSS_BENCHMARK_PATH)/
	for model in $(shell ls models/$*); do $(MAKE) -B -C models/$*/$$model tflow.cross.dat; done

benchmarks-clean:
	$(MAKE) -C models clean
