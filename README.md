# triNNity-demos

This repository has demonstrations of the workflow of using the triNNity DNN
toolchain on several popular models and datasets.

Broadly speaking, there are three steps involved in optimizing your DNN with
triNNity: extracting microbenchmarking scenarios from the Caffe model, building a
cost model by performing the microbenchmarking, and running the triNNity
optimizer to generate a high-performance executable to perform inference.

The full flow is demonstrated in the `Makefile` for every model in the `models`
subdirectory.

If you intend to train or prune models, you will need to download the dataset
you're interested in. Just run `get.py` in the relevant subdirectory in
`datasets`.

## Creating the build environment

You can create the build environment manually, or using Docker.

If you are using Docker, at the top level in this repository say

`docker build -t trinnity-demos -f Dockerfile.cpu .`

There is also a `Dockerfile.gpu` for a CUDA GPU setup.

You may need to give your user permission to use docker with

`gpasswd -a $USER docker`

To get a shell in the build environment, say

`docker run -i -t trinnity-demos /bin/bash`

Because Docker does not support disabling the cache for individual
build steps, the final build steps that build and install the triNNity
DNN toolkit must be run outside Docker (or else you will be stuck with
stale versions of the software in the Docker cache).

Inside the build environment, you can say `make update` to update the
triNNity library, compiler, and optimizer. To update triNNity-caffe say
either `make update-caffe-cpu` or `make update-caffe-gpu`, depending on
whether you are using the CPU or GPU Dockerfile.

## Microbenchmarking DNN Layers and Building Cost Models

This project ships with some pregenerated cost models in the `cost-models`
directory.

In order to build new cost models, the `triNNity-benchmarks` project can be
used to perform the microbenchmarking process and generate the cost models. For
more details, see the documentation in that project.

To generate the scenario definition file that says what layers should be
microbenchmarked, you can say `make $PROBLEM-scenarios` in this repository,
where `$PROBLEM` is the name of a subdirectory in `models`. For example: `make
imagenet-scenarios` will generate a file `imagenet-scenarios` that can be
passed to the benchmarking process.

## Benchmarking Inference Performance

To run the full network benchmarking process, say `make $PROBLEM-benchmarks`.
For example, to run the CIFAR10 benchmarking, say `make cifar10-benchmarks`.

When the benchmarking process completes, you can generate a summary CSV file
from the performance data by saying `make -C models $PROBLEM.csv`.

NB: to force make to regenerate files (for example, the .csv file) say `make -B` in addition
to the other arguments.

To run only a specific subset of benchmarks, say `make $LIBRARY-benchmarks-$PROBLEM`.
For example, to run the ImageNet benchmarking using ARMCL, say `make armcl-benchmarks-imagenet`.

### Selecting a Cost Model for Network Construction

The `cost-models` directory contains some pregenerated cost models for the
optimizer to use when building networks. To specify which cost model to use,
the following variables are used on the command line.

- `COST_MODEL=*i7-8700K*` -- Any wild-card matching a directory name in `cost-models`
- `COST_MODEL_VAR=*single*` -- Any wild-card matching a subdirectory in `cost-models/<some platform>`

For example, to run the host benchmarks but optimize according to the A57 cost
model, you can say `make -B cifar10-benchmarks COST_MODEL=*A57* COST_MODEL_VAR=*single*`

### Controlling Benchmark Execution

You can run the benchmarks using specific subsets of cores in the machine and
for some number of runs using the following command line variables.

- `RUNS=25` -- Do 25 runs
- `CORES=4` -- Use 4 cores
- `CORE_IDS=0,1,2,3` -- specifically, use cores 0-3 (uses taskset syntax)

### Cross-Compilation Target Configuration

Special rules are provided for cross compilation with the `cross-` prefix. To
run the full cross-compiled benchmarks you can use the top-level Makefile as
usual, but you need to specify two extra variables.

- `CROSS_BENCHMARK_HOST` -- needs to be in user@host format
- `CROSS_BENCHMARK_PATH` -- relative path from $HOME for user@host to store the built executables and benchmark metadata.

For example, to build the benchmarks cross-compiled for the Cortex-A57 machine
`devkit`, you can say `make -B cross-armcl-benchmarks
CROSS_BENCHMARK_HOST=ubuntu@devkit CROSS_BENCHMARK_PATH=bench-scratch-space`

To control the cross-compilation target, the following command line variables
may be used.

- `CROSS_CPU=cortex-a57` -- any valid option for `gcc -mcpu=` will work here
- `CROSS_CXX=aarch64-linux-gnu-g++` -- use this cross-compiler
- `CROSS_ARCH_FLAGS=-static -Wall -Wno-error=coverage-mismatch --pedantic -fopenmp -faligned-new -fno-stack-protector` -- build with these flags for cross compilation

For example, to build for the Cortex-A53 machine `rpi3` in 32-bit mode, you can say `make -B cross-armcl-benchmarks
CROSS_BENCHMARK_HOST=ubuntu@rpi3 CROSS_BENCHMARK_PATH=bench-scratch-space
CROSS_CPU=cortex-a53 CROSS_CXX=arm-linux-gnueabihf-g++`
