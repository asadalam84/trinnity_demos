from imutils.video import FileVideoStream
from imutils.video import FPS
import numpy as np
import argparse
import imutils
import time
import cv2

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-p", "--prototxt", required=True,
  help="path to Caffe 'deploy' prototxt file")
ap.add_argument("-m", "--model", required=True,
  help="path to Caffe pre-trained model")
ap.add_argument("-l", "--labels", required=True,
  help="path to dataset labels file")
ap.add_argument("-f", "--file", required=True,
  help="path to input video file")
ap.add_argument("-x", "--width", type=int, required=True,
  help="width of input to network")
ap.add_argument("-y", "--height", type=int, required=True,
  help="height of input to network")
ap.add_argument("-c", "--confidence", type=float, default=0.2,
  help="minimum probability to filter weak detections")
ap.add_argument("-u", "--movidius", type=bool, default=False,
  help="boolean indicating if the Movidius NCS should be used")
args = vars(ap.parse_args())

# initialize the list of class labels
# then generate a set of colors for each class
CLASSES = []

with open(args["labels"], "r") as classes_file:
  CLASSES = [line.strip() for line in classes_file.readlines()]

COLORS = np.random.uniform(0, 255, size=(len(CLASSES), 3))

# load our serialized model from disk
print("[INFO] loading model...")
net = cv2.dnn.readNetFromCaffe(args["prototxt"], args["model"])

# specify the target device as the Myriad processor on the NCS
if args["movidius"]:
  net.setPreferableTarget(cv2.dnn.DNN_TARGET_MYRIAD)

# initialize the video stream, allow the cammera sensor to warmup,
# and initialize the FPS counter
print("[INFO] starting video stream...")
vs = FileVideoStream(args["file"]).start()
time.sleep(2.0)
fps = FPS().start()

# loop over the frames from the video stream
while True:
  # grab the frame from the threaded video stream and resize it
  # to have a maximum width of 400 pixels
  frame = vs.read()
  frame = imutils.resize(frame, width=400)

  # grab the frame dimensions and convert it to a blob
  (h, w) = frame.shape[:2]
  blob = cv2.dnn.blobFromImage(frame, 1, (args["width"], args["height"]), (123.675, 116.28, 103.53))

  # pass the blob through the network and obtain the detections and
  # predictions
  net.setInput(blob)
  predictions = net.forward()

  # get the top 5 predictions
  idxs = np.argsort(predictions[0])[::-1][:5]

  # loop over the top 5 predictions
  for (i, idx) in enumerate(idxs):
    # draw the top prediction on the input image
    if i == 0:
      text = "Label: {}, {:.2f}%".format(CLASSES[idx],
        predictions[0][idx] * 100)
      cv2.putText(frame, text, (5, 25),  cv2.FONT_HERSHEY_SIMPLEX,
        0.7, (0, 0, 255), 2)

    # display the predicted label + associated probability to the
    # console
    print("[INFO] {}. label: {}, probability: {:.5}".format(i + 1,
      CLASSES[idx], predictions[0][idx]))

  # display the output image
  cv2.imshow("Image", frame)

  key = cv2.waitKey(1) & 0xFF

  # if the `q` key was pressed, break from the loop
  if key == ord("q"):
    break

  # update the FPS counter
  fps.update()

# stop the timer and display FPS information
fps.stop()
print("[INFO] elasped time: {:.2f}".format(fps.elapsed()))
print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))

# cleanup
vs.stop()
vs.stream.release()

cv2.destroyAllWindows()
